/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Actions;

import com.mycompany.gustatifweb.ActionServlet;
import static com.mycompany.gustatifweb.ActionServlet.attrListeProduit;
import static com.mycompany.gustatifweb.ActionServlet.attrRestaurant;
import static com.mycompany.gustatifweb.ActionServlet.clientConfirmationCommande;
import static com.mycompany.gustatifweb.ActionServlet.clientListeRestaurants;
import static com.mycompany.gustatifweb.ActionServlet.indexPage;
import static com.mycompany.gustatifweb.ActionServlet.typeLivreur;
import static com.mycompany.gustatifweb.ActionServlet.userIDAttribute;
import static com.mycompany.gustatifweb.ActionServlet.userRestauCourantAttribute;
import static com.mycompany.gustatifweb.ActionServlet.usernameAttribute;
import static com.mycompany.gustatifweb.ActionServlet.usertypeAttribute;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import metier.modele.Client;
import metier.modele.Commande;
import metier.modele.CommandeProduit;
import metier.modele.Livreur;
import metier.modele.Produit;
import metier.modele.Restaurant;
import metier.service.ServiceMetier;
import util.exception.LivreurUnavailableException;

/**
 *
 * @author Vincent Falconieri
 */
public class ActiviteCommande {

    ServiceMetier service;
    double poidsCourant;
    double prixCourant;

    public double getPoidsCourant() {
        return poidsCourant;
    }

    public ActiviteCommande(ServiceMetier service) {
        this.service = service;
        this.poidsCourant = 0;
        this.prixCourant = 0;
    }

    /**
     * Permet de terminer une commande en créant la commande associée au panier
     * d'un utilisater
     *
     * @param session
     * @param request
     * @return true si s'est correctement déroulée, false sinon. Ajout d'un
     * message d'erreur dans la requête via un attribut "error"
     */
    public boolean passerCommande(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        //Récupérer les informations
        String idRestaurantCommandeSTRING = request.getParameter("idRestaurant");
        long idRestaurantCommandeLONG = -1;
        Restaurant restaurantCommande = null;
        long idClientCommande = (long) session.getAttribute(userIDAttribute);
        Client clientCommande = null;
        List<Produit> listeProduitsRestaurant = null;

        // Parsing des informations
        try {
            idRestaurantCommandeLONG = Long.parseLong(idRestaurantCommandeSTRING);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteCommande : " + ex);
            ActionServlet.setError(request, "ERREUR : ID de restaurant invalide.", ActionServlet.clientListeRestaurants);
            enErreur = true;
        }
        //TODO : vérification sur l'ID du restaurant ? 

        //Récupération du restaurant
        try {
            restaurantCommande = service.trouverRestaurantParId(idRestaurantCommandeLONG);
            //DEBUG //
            System.err.println("ActiviteCommande - Le restaurant trouvé est : " + restaurantCommande);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteCommande : " + ex);
            ActionServlet.setError(request, "ERREUR : ID de restaurant inconnu.", ActionServlet.clientListeRestaurants);
            enErreur = true;
        }

        //Récupération de l'utilisateur
        try {
            clientCommande = service.trouverClientParId(idClientCommande);
            //DEBUG //
            System.err.println("Le client trouvé est : " + clientCommande);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteCommande : " + ex);
            ActionServlet.setError(request, "ERREUR : ID de client inconnu.", ActionServlet.indexPage);
            enErreur = true;
        }

        //Récupération de la liste des produits du restaurant
        try {
            ActiviteProduit activiteProduits = new ActiviteProduit(service);
            listeProduitsRestaurant = activiteProduits.printProduitsRestaurant(idRestaurantCommandeLONG);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteCommande : " + ex);
            ActionServlet.setError(request, "ERREUR : impossible de récupérer la liste des produits d'un restaurant.", ActionServlet.indexPage);
            enErreur = true;
        }

        if (!enErreur) {

            //Création de la commande
            ActiviteCommande activiteCommandes = new ActiviteCommande(service);
            Commande commandeCourante = activiteCommandes.createCommande(listeProduitsRestaurant, clientCommande, restaurantCommande, request);

            //Ajout de la commande à la BDD
            try {
                //DEBUG //
                System.err.println("On envoit : " + clientCommande + " " + commandeCourante + " " + activiteCommandes.getPoidsCourant());

                //Créer la commande et redirige
                service.creerCommande(commandeCourante, clientCommande, activiteCommandes.getPoidsCourant());

                //Message de confirmation
                ActionServlet.setMessage(request, "Votre commande a bien été créée.", ActionServlet.clientConfirmationCommande);

            } catch (LivreurUnavailableException ex) {
                System.err.println("Erreur - ActiviteCommande : " + ex);
                ActionServlet.setError(request, "Erreur - Impossible de passer la commande: livreur indisponible. (Vérifiez votre connexion Internet également)", ActionServlet.indexPage);
                enErreur = true;
            } catch (Exception ex) {
                System.err.println("Erreur - ActiviteCommande : " + ex);
                ActionServlet.setError(request, "Erreur - Impossible de passer la commande: erreur interne. (Vérifiez votre connexion Internet également)", ActionServlet.indexPage);
                enErreur = true;
            }
        }

        return enErreur;
    }

    /**
     * Permet de récupérer les détails de la dernière commande du client de
     * session
     *
     * @param session
     * @param request
     * @return true si s'est correctement déroulée, false sinon. Ajout d'un
     * message d'erreur dans la requête via un attribut "error"
     */
    public boolean getDetailsLastCommande(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        //Récupère les informations
        List<Commande> listeCommandeEnCours = service.listeCommandesEnCours();
        long idClientCommande = (long) session.getAttribute(userIDAttribute);
        Client clientCommande = null;
        Commande commandeCourante = null;

        //Récupération de l'utilisateur
        try {
            clientCommande = service.trouverClientParId(idClientCommande);
            //DEBUG //
            System.err.println("Le client trouvé est : " + clientCommande);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteCommande : " + ex);
            ActionServlet.setError(request, "ERREUR : ID de client inconnu.", ActionServlet.indexPage);
            enErreur = true;
        }

        //Récupération de la commande
        try {
            commandeCourante = getLastCommandeEnCours(listeCommandeEnCours, clientCommande);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteCommande : " + ex);
            ActionServlet.setError(request, "ERREUR : impossible de récupérer la dernière commande.", ActionServlet.indexPage);
            enErreur = true;
        }

        //Envoi à la Servlet par request des attributs nécessaires.
        if (!enErreur && commandeCourante != null) {
            //TODO = A mettre dans une méthode à part ? 
            request.setAttribute(ActionServlet.attrCommande, commandeCourante);
            request.setAttribute(ActionServlet.attrRestaurant, commandeCourante.getRestaurant());
            //Enpaquetage des données dans la requête
            ActionServlet.setMessage(request, "Les détails de votre dernière commande ont été correctement récupérés.", ActionServlet.clientConfirmationCommande);
        } else if (commandeCourante == null) { //La commande n'existe pas.
            ActionServlet.setError(request, "Vous n'avez passé aucune commande.", ActionServlet.mainPageClient);
            enErreur = true;
        }

        return enErreur;
    }

    /* Permet de récupérer les détails d'une commande du client de session
     *
     * @param session
     * @param request
     * @return true si s'est correctement déroulée, false sinon. Ajout d'un
     * message d'erreur dans la requête via un attribut "error"
     */
    public boolean getDetailsUneCommande(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        //Récupère les informations
        String idCommandeSTRING = request.getParameter("idCommandeDetail");
        long idCommandeLONG = -1;
        Commande commandeCourante = null;
        long idClientCommande = (long) session.getAttribute(userIDAttribute);
        Client clientCommande = null;

        //Parsing ID commande
        try {
            idCommandeLONG = Long.parseLong(idCommandeSTRING);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteCommande : " + ex);
            ActionServlet.setError(request, "ERREUR : choix de commande impossible, ID invalide.", ActionServlet.clientListeHistorique);
            enErreur = true;
        }

        //Récupération de l'utilisateur
        try {
            clientCommande = service.trouverClientParId(idClientCommande);
            //DEBUG //
            System.err.println("Le client trouvé est : " + clientCommande);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteCommande : " + ex);
            ActionServlet.setError(request, "ERREUR : ID de client inconnu.", ActionServlet.indexPage);
            enErreur = true;
        }

        //Récupération de la commande
        try {
            commandeCourante = service.trouverCommandeParId(idCommandeLONG);
            //DEBUG //
            System.err.println("La commande trouvée est : " + commandeCourante);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteCommande : " + ex);
            ActionServlet.setError(request, "ERREUR : ID de commande inconnu.", ActionServlet.indexPage);
            enErreur = true;
        }

        //Envoi à la Servlet par request des attributs nécessaires.
        if (!enErreur && commandeCourante != null) {
            //TODO = A mettre dans une méthode à part ? 
            request.setAttribute(ActionServlet.attrCommande, commandeCourante);
            request.setAttribute(ActionServlet.attrRestaurant, commandeCourante.getRestaurant());
            //Enpaquetage des données dans la requête
            ActionServlet.setMessage(request, "Les détails de la commande demandée ont été correctement récupérés.", ActionServlet.clientListeHistorique);
        } else if (commandeCourante == null) { //La commande n'existe pas.
            ActionServlet.setError(request, "Vous n'avez passé aucune commande.", ActionServlet.mainPageClient);
            enErreur = true;
        }

        return enErreur;
    }

    /**
     * Permet de récupérer la dernière commande en cours d'un client
     * particulier, dans la liste passée en paramètre
     *
     * @param listeCommandeEnCours
     * @param deCeClient
     * @return
     */
    protected Commande getLastCommandeEnCours(List<Commande> listeCommandeEnCours, Client deCeClient) {
        //On créé quelques variables utiles
        long idCible = deCeClient.getId();
        Commande lastCommande = null;

        for (Commande cm : listeCommandeEnCours) { //Pour chaque commande de la liste
            if (cm.getClient().getId() == idCible) { //On vérifie si c'est une commande du client
                if (lastCommande == null) { // On en a pas trouvé, donc on la stocke, de fait.
                    lastCommande = cm;
                } else if (lastCommande.getDate_initiale().before(cm.getDate_initiale())) { //Celle stockée est plus ancienne
                    lastCommande = cm;
                }
            }
        }
        return lastCommande;
    }

    /**
     * Permet de récupérer la dernière commande en cours tous clients confondus,
     * dans la liste passée en paramètre
     *
     * @param listeCommandeEnCours
     * @return
     */
    protected Commande getLastCommandeEnCours(List<Commande> listeCommandeEnCours) {
        Commande lastCommande = null;
        for (Commande cm : listeCommandeEnCours) { //Pour chaque commande de la liste
            if (lastCommande == null) { // On en a pas trouvé, donc on la stocke, de fait.
                lastCommande = cm;
            } else if (lastCommande.getDate_initiale().before(cm.getDate_initiale())) { //Celle stockée est plus ancienne
                lastCommande = cm;
            }
        }
        return lastCommande;
    }

    /**
     * Créer une commande à partir d'une liste de produit, un client, un
     * restaurant.
     *
     * @param listeProduits
     * @param client
     * @param restaurant
     * @param request
     * @return
     */
    public Commande createCommande(List<Produit> listeProduits, Client client, Restaurant restaurant, HttpServletRequest request) {
        //On remet le poids à zéro (normalement pas besoin, mais au cas où)
        poidsCourant = 0;
        prixCourant = 0;
        ArrayList<CommandeProduit> aMettreDansCommande = new ArrayList();

        //Pour chaque produit de la liste, on regarde si un argument existe.
        //Si il existe, on récupère la quantité associée
        for (Produit p : listeProduits) {
            String quantiteCouranteSTRING = request.getParameter(p.getId().toString());
            int quantiteCouranteINT = 0;

            try {
                quantiteCouranteINT = Integer.parseInt(quantiteCouranteSTRING);
            } catch (Exception ex) {
                System.err.println(ex);
            }

            if (quantiteCouranteINT > 0) {
                //DEBUG //
                System.err.println("On compte : " + quantiteCouranteINT + " de : " + p.getDenomination());
                //On crée un couple commandeProduit (parce qu'ils l'ont pensé comme ça Oo)
                CommandeProduit cp = new CommandeProduit(quantiteCouranteINT, p);
                //On l'ajoute à la commande
                //commandeRetour.addProduits(cp); //SI on avait pas le problème du poids au constructeur de la commande.
                aMettreDansCommande.add(cp);

                //On calcul et on ajoute le poids
                poidsCourant += p.getPoids() * quantiteCouranteINT;
                prixCourant += p.getPrix() * quantiteCouranteINT;
                //DEBUG : 
                System.err.println("Ce qui monte le poids à : " + poidsCourant + "g");
            }
        }

        //On créé la commande avec le poids connu 
        Commande commandeRetour = new Commande(Commande.ETAT_EN_COURS, new Date(System.currentTimeMillis()), null, prixCourant, client, restaurant);

        //On rempli la comande avec les CommandeProduit généré précédemment
        for (CommandeProduit cp : aMettreDansCommande) {
            commandeRetour.addProduits(cp);
        }

        //DEBUG //
        System.err.println("ACTIVITECOMMANDE : On envoit donc la commande suivante : " + commandeRetour);
        return commandeRetour;
    }

    /**
     * Permet de récupérer les données administratives pour affichage sur une
     * carte
     *
     * @return
     */
    public boolean getDetailsAdministration(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        List<Client> listeClients = null;
        List<Commande> listeCommandes = null;
        List<Livreur> listeLivreurs = null;
        List<Restaurant> listeRestaurants = null;

        //Récupère les informations
        try {
            listeClients = service.listeClients();
            listeCommandes = service.listeCommandesEnCours();
            listeLivreurs = service.listeLivreurs();
            listeRestaurants = service.listeRestaurants();
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteCommande : " + ex);
            ActionServlet.setError(request, "ERREUR : récupération des informations impossible.", ActionServlet.indexPage);
            enErreur = true;
        }

        //Envoi à la Servlet par request des attributs nécessaires.
        if (!enErreur) {
            //TODO = A mettre dans une méthode à part ? 
            request.setAttribute(ActionServlet.attrListeClient, listeClients);
            request.setAttribute(ActionServlet.attrListeCommande, listeCommandes);
            request.setAttribute(ActionServlet.attrListeLivreur, listeLivreurs);
            request.setAttribute(ActionServlet.attrListeRestaurant, listeRestaurants);

            //Enpaquetage des données dans la requête
            ActionServlet.setMessage(request, "Les détails de la commande demandée ont été correctement récupérés.", ActionServlet.clientListeHistorique);
        } else if (listeCommandes.isEmpty()) { //La commande n'existe pas.
            ActionServlet.setError(request, "Aucune commande en cours.", ActionServlet.mainPageClient);
            enErreur = true;
        }

        return enErreur;
    }

}
