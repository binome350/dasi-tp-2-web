package Actions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mycompany.gustatifweb.ActionServlet;
import static com.mycompany.gustatifweb.ActionServlet.attrListeProduit;
import static com.mycompany.gustatifweb.ActionServlet.attrListeRestaurant;
import static com.mycompany.gustatifweb.ActionServlet.attrRestaurant;
import static com.mycompany.gustatifweb.ActionServlet.clientListeProduits;
import static com.mycompany.gustatifweb.ActionServlet.clientListeRestaurants;
import static com.mycompany.gustatifweb.ActionServlet.typeUser;
import static com.mycompany.gustatifweb.ActionServlet.userIDAttribute;
import static com.mycompany.gustatifweb.ActionServlet.userRestauCourantAttribute;
import static com.mycompany.gustatifweb.ActionServlet.usernameAttribute;
import static com.mycompany.gustatifweb.ActionServlet.usertypeAttribute;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import metier.modele.Client;
import metier.modele.Commande;
import metier.modele.Produit;
import metier.modele.Restaurant;
import metier.service.ServiceMetier;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Vincent Falconieri
 */
public class ActiviteRestaurant {

    ServiceMetier service;

    public ActiviteRestaurant(ServiceMetier service) {
        this.service = service;
    }

    /**
     * Permet d'ajouter un restaurant à la session de l'utilisateur lors d'un
     * processus de commande. (donnée temporaire lors de la prise de commande)
     *
     * @param session
     * @param request
     * @return true si s'est correctement déroulée, false sinon. Ajout d'un
     * message d'erreur dans la requête via un attribut "error"
     */
    public boolean setRestaurantCourant(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        //Récupère les informations
        String idRestaurantChoisiString = request.getParameter("idResto");

        //Parsing des informations
        long idRestaurantChoisiLong = -1;
        try {
            idRestaurantChoisiLong = Long.parseLong(idRestaurantChoisiString);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteRestaurant : " + ex);
            ActionServlet.setError(request, "ERREUR : choix de restaurant impossible, ID invalide.", ActionServlet.clientListeRestaurants);
            enErreur = true;
        }

        //Stockage du restaurant dans la session
        if (!enErreur) {
            session.setAttribute(userRestauCourantAttribute, idRestaurantChoisiLong); //On stocke l'ID du restaurant qu'il a choisi.
            //On transmet un message d'authentification
            ActionServlet.setMessage(request, "Votre choix de restaurant a bien été pris en compte.", ActionServlet.clientListeProduits);
        }

        return enErreur;
    }

    /**
     * Permet de récupérer les détails du restaurant passé par la session
     *
     * @param session
     * @param request
     * @return true si s'est correctement déroulée, false sinon. Ajout d'un
     * message d'erreur dans la requête via un attribut "error"
     */
    public boolean getDetailsRestaurant(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        //Récupère les informations
        Long idRestaurant = Long.parseLong(request.getParameter("id"));
        Restaurant restaurantDemande = null;

        //DEBUG //
        System.err.println("ActiviteRestaurant - ID dans print Restaurant : " + idRestaurant);

        //Récupération du restaurant
        try {
            restaurantDemande = service.trouverRestaurantParId(idRestaurant);
            //DEBUG //
            System.err.println("ActiviteCommande - Le restaurant trouvé est : " + restaurantDemande);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteRestaurant : " + ex);
            ActionServlet.setError(request, "ERREUR : ID de restaurant inconnu.", ActionServlet.clientListeRestaurants);
            enErreur = true;
        }

        //Envoi à la Servlet par request des attributs nécessaires.
        if (!enErreur && restaurantDemande != null) {
            //TODO = A mettre dans une méthode à part ? 
            request.setAttribute(ActionServlet.attrRestaurant, restaurantDemande);
            //Enpaquetage des données dans la requête
            ActionServlet.setMessage(request, "Les détails du restaurant demandé ont été correctement récupérés.", ActionServlet.indexPage);
        } else if (restaurantDemande == null) {
            ActionServlet.setError(request, "ERREUR : aucun restaurant ne correspond à cet ID.", ActionServlet.mainPageClient);
            enErreur = true;
        }

        return enErreur;
    }

    /**
     * Permet de lister les restaurants à envoyer à la page
     *
     * @param session
     * @param request
     * @return true si s'est correctement déroulée, false sinon. Ajout d'un
     * message d'erreur dans la requête via un attribut "error"
     */
    public boolean listerRestaurants(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        //Récupération des données
        List<Restaurant> listeRestaurants = service.listeRestaurants();

        //Enpaquetage des données dans la requête
        ActionServlet.setMessage(request, "La listes des restaurants ont été correctement récupérés.", ActionServlet.clientListeRestaurants);
        //TODO = A mettre dans une méthode à part ? 
        request.setAttribute(attrListeRestaurant, listeRestaurants);

        return enErreur;
    }

    public Restaurant printUnRestaurant(Long id) {
        //DEBUG //
        System.err.println("ID dans print Restaurant : " + id);
        Restaurant unResto = service.trouverRestaurantParId(id);
        return unResto;
    }
}
