package Actions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mycompany.gustatifweb.ActionServlet;
import static com.mycompany.gustatifweb.ActionServlet.attrErreur;
import static com.mycompany.gustatifweb.ActionServlet.attrListeProduit;
import static com.mycompany.gustatifweb.ActionServlet.attrMessage;
import static com.mycompany.gustatifweb.ActionServlet.attrRestaurant;
import static com.mycompany.gustatifweb.ActionServlet.clientListeRestaurants;
import static com.mycompany.gustatifweb.ActionServlet.indexPage;
import static com.mycompany.gustatifweb.ActionServlet.typeUser;
import static com.mycompany.gustatifweb.ActionServlet.userIDAttribute;
import static com.mycompany.gustatifweb.ActionServlet.userRestauCourantAttribute;
import static com.mycompany.gustatifweb.ActionServlet.usernameAttribute;
import static com.mycompany.gustatifweb.ActionServlet.usertypeAttribute;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import metier.modele.Client;
import metier.modele.Restaurant;
import metier.modele.Produit;
import metier.service.ServiceMetier;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Vincent Falconieri
 */
public class ActiviteProduit {

    ServiceMetier service;

    public ActiviteProduit(ServiceMetier service) {
        this.service = service;
    }

    /**
     * Permet de lister les produits à envoyer à la page en fonction d'un id de
     * restaurant
     *
     * @param session
     * @param request
     * @return true si s'est correctement déroulée, false sinon. Ajout d'un
     * message d'erreur dans la requête via un attribut "error"
     */
    public boolean listerProduits(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        //Récupère et Parse des informations
        long idRestaurantChoisiLong = -1;
        try {
            idRestaurantChoisiLong = (long) session.getAttribute(userRestauCourantAttribute);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteProduit : " + ex);
            ActionServlet.setError(request, "ERREUR : présentation des produits impossible, aucun restaurant choisi.", ActionServlet.clientListeRestaurants);
            enErreur = true;
        }

        //Verification validité de l'id de restaurant
        if (idRestaurantChoisiLong < 0) {
            //TODO Ajouter une vérification sur la borne haute.
            ActionServlet.setError(request, "ERREUR : choix de produits impossible, ID de restaurant invalide.", ActionServlet.clientListeRestaurants);
            enErreur = true;
        }

        //DEBUG // System.err.println("ID Restaurant actuel = " + idRestaurantChoisi);
        // ==== Ajout des informations utiles à la session ====
        ActiviteRestaurant activiteRestau = new ActiviteRestaurant(service);

        //Récupération des données
        Restaurant restaurant = activiteRestau.printUnRestaurant(idRestaurantChoisiLong);
        List<Produit> listeProduits = this.printProduitsRestaurant(idRestaurantChoisiLong);

        //Enpaquetage des données dans la requête
        ActionServlet.setMessage(request, "La listes des produits du restaurant cible a été correctement récupéré.", ActionServlet.clientListeProduits);
        //TODO = A mettre dans une méthode à part ? 
        request.setAttribute(attrRestaurant, restaurant);
        request.setAttribute(attrListeProduit, listeProduits);
        
        return enErreur;
    }

    public List<Produit> printProduitsRestaurant(Long idResto) {
        //DEBUG //
        System.err.println("ID dans print ProduitsRestaurant : " + idResto);

        Restaurant unResto = service.trouverRestaurantParId(idResto);

        //Conversion collection -> Liste
        Collection<Produit> coll = unResto.getProduits();
        List<Produit> list;
        if (coll instanceof List) {
            list = (List) coll;
        } else {
            list = new ArrayList(coll);
        }
        return list;
    }
}
