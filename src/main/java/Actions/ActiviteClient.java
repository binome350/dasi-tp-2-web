/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Actions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mycompany.gustatifweb.ActionServlet;
import static com.mycompany.gustatifweb.ActionServlet.connexionClientPage;
import static com.mycompany.gustatifweb.ActionServlet.mainPageClient;
import static com.mycompany.gustatifweb.ActionServlet.typeUser;
import static com.mycompany.gustatifweb.ActionServlet.userIDAttribute;
import static com.mycompany.gustatifweb.ActionServlet.usernameAttribute;
import static com.mycompany.gustatifweb.ActionServlet.usertypeAttribute;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import metier.modele.Client;
import metier.modele.Commande;
import metier.modele.Produit;
import metier.service.ServiceMetier;

/**
 *
 * @author Vincent Falconieri
 */
public class ActiviteClient {

    ServiceMetier service;

    public ActiviteClient(ServiceMetier service) {
        this.service = service;
    }

    /**
     * Permet d'inscrire un client sur l'application.
     *
     * @param session
     * @param request
     * @return true si s'est correctement déroulée, false sinon. Ajout d'un
     * message d'erreur dans la requête via un attribut "error"
     */
    public boolean inscrireClient(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        //Récupère les ID/mdp
        String nom = request.getParameter("nom");
        String prenom = request.getParameter("prenom");
        String email = request.getParameter("email");
        String adresse = request.getParameter("adresse");

        //DEBUG BLOCK //
        System.err.println("nom :" + nom);
        System.err.println("prenom :" + prenom);
        System.err.println("email :" + email);
        System.err.println("adresse :" + adresse);
        //FIN DEBUG BLOCK //

        // Vérification des données rentrées (validité check)
        if (nom != null && prenom != null && email != null && adresse != null) {

            // VERIFICATION DE L'EMAIL
            if (!Pattern.matches("^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)+$", email)) {
                //DEBUG //
                System.err.println("ERREUR : Activite Client - email ne match pas.");
                enErreur = true;
            }

            //VERIFICATION DU NOM/PRENOM/ADRESSE
            // TODO 

            /* Premier essai effectuée avec une regex pour gérer les accents, etc. Peut être trop compliqué pour l'intérêt que ça a.
            String regexAlphanumerique = "/^[a-zA-ZáàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ._\\s-]{5,60}$/";
            //Alphanumérique pour le nom
            if (nom.matches(regexAlphanumerique) == false) {
                isCorrect = false;
            }*/
        } else {
            //Il n'y a pas tous les arguments
            enErreur = true;
        }

        //Vérification logique des données rentrées (existence check)
        if (!enErreur) {
            //Vérification de l'existence dans la base
            Client userCourant = service.trouverClient(email);

            if (userCourant == null) { //L'user n'existe pas.
                //Création de l'objet client
                Client newUser = new Client(nom, prenom, email, adresse);

                try {
                    //Tentative d'ajout dans la base
                    service.creerClient(newUser);
                    //Retour à la fonction
                    ActionServlet.setMessage(request, "L'utilisateur a été inscrit.", ActionServlet.connexionClientPage);
                } catch (Exception ex) { //erreur durant le traitement
                    ActionServlet.setError(request, "ERREUR : L'utilisateur n'a pas été inscrit.", ActionServlet.inscriptionPage);
                    enErreur = true;
                    System.err.println("ERREUR ActiviteClient - " + ex);
                }
            } else {//L'user existe déjà
                ActionServlet.setError(request, "ERREUR : L'utilisateur existe déjà.", ActionServlet.inscriptionPage);
                enErreur = true;
            }
        } else {
            ActionServlet.setError(request, "ERREUR : Entrées incorrectes.", ActionServlet.inscriptionPage);
            enErreur = true;
        }

        return enErreur;
    }

    /**
     * Permet de connecter un client sur l'application.
     *
     * @param session
     * @param request
     * @return true si s'est correctement déroulée, false sinon. Ajout d'un
     * message d'erreur dans la requête via un attribut "error"
     */
    public boolean connecterClient(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        //Récupère les ID/mdp
        String usernameFormulaire = request.getParameter("identifiant");
        String mdp = request.getParameter("motdepasse");

        //DEBUG ZONE //
        System.err.println("user :" + usernameFormulaire);
        System.err.println("mdp :" + mdp);
        //FIN DEBUG ZONE //

        //On check si le client existe
        Client userObject = service.trouverClient(usernameFormulaire);

        if (userObject != null) { //L'user existe
            //On ajoute des informations à sa session  = on le connecte
            session.setAttribute(usernameAttribute, usernameFormulaire); //On lui donne un nom
            session.setAttribute(usertypeAttribute, typeUser); //on lui donne un type
            session.setAttribute(userIDAttribute, userObject.getId()); //on lui attribut son objet de la BD

            //DEBUG//
            System.err.println("ActiviteClient - Connexion : ON STOCKE DANS LA SESSION L'ID d'user : " + userObject.getId());

            //On transmet un message d'authentification
            ActionServlet.setMessage(request, "L'utilisateur a été connecté.", ActionServlet.mainPageClient);

        } else { //L'user n'existe pas.
            ActionServlet.setError(request, "L'utilisateur spécifié n'existe pas.", ActionServlet.connexionClientPage);
            enErreur = true;
        }

        return enErreur;
    }

    /**
     * Permet de déconnecter un client/livreur/Admin sur l'application.
     *
     * @param session
     * @param request
     * @return true si s'est correctement déroulée, false sinon. Ajout d'un
     * message d'erreur dans la requête via un attribut "error"
     */
    public boolean deconnecterClient(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        try {
            session.setAttribute(usernameAttribute, null); //On lui retire son nom
            session.setAttribute(usertypeAttribute, null); //on lui retire son type
            session.setAttribute(userIDAttribute, null); //on lui retire son objet de la BD

            //On transmet un message de validation
            ActionServlet.setMessage(request, "Vous avez été deconnecté.", ActionServlet.indexPage);

        } catch (Exception ex) {
            System.err.println("ERREUR - ActiviteClient : Déconexion impossible.");
            ActionServlet.setError(request, "Deconnexion impossible. Contactez l'administrateur.", ActionServlet.indexPage);
            enErreur = true;
        }

        return enErreur;
    }
    
     /* Permet de récupérer les détails d'une commande du client de session
     *
     * @param session
     * @param request
     * @return true si s'est correctement déroulée, false sinon. Ajout d'un
     * message d'erreur dans la requête via un attribut "error"
     */
    public boolean getHistoriqueCommande(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        //Récupère les informations
        List<Commande> listeCommandes = null;
        long idClientCommande = (long) session.getAttribute(userIDAttribute);
        Client clientCommande = null;

        //Récupération de l'utilisateur
        try {
            clientCommande = service.trouverClientParId(idClientCommande);
            //DEBUG //
            System.err.println("Le client trouvé est : " + clientCommande);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteCommande : " + ex);
            ActionServlet.setError(request, "ERREUR : ID de client inconnu.", ActionServlet.indexPage);
            enErreur = true;
        }

        //Récupération des commande
        try {
            listeCommandes = this.printListeHistorique(clientCommande);
            //DEBUG //
            System.err.println("Les commande trouvées sont : " + listeCommandes);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteCommande : " + ex);
            ActionServlet.setError(request, "ERREUR : ID de commande inconnu.", ActionServlet.indexPage);
            enErreur = true;
        }

        //Tri des commandes
        // TO DO = faire le trie des objets de la liste.
        //Envoi à la Servlet par request des attributs nécessaires.
        if (!enErreur && listeCommandes != null && !listeCommandes.isEmpty()) {
            //TODO = A mettre dans une méthode à part ? 
            request.setAttribute(ActionServlet.attrListeCommande, listeCommandes);
            //Enpaquetage des données dans la requête
            ActionServlet.setMessage(request, "Les détails de l'historique des commande a été correctement récupéré.", ActionServlet.clientListeHistorique);
        } else if (listeCommandes == null | listeCommandes.isEmpty()) { //La commande n'existe pas.
            ActionServlet.setError(request, "Vous n'avez passé aucune commande.", ActionServlet.mainPageClient);
            enErreur = true;
        }

        return enErreur;
    }

    public List<Commande> printListeHistorique(Client client) {
        Collection<Commande> listeHistorique = client.getCommandes();

        //Conversion collection -> Liste
        List<Commande> list;
        if (listeHistorique instanceof List) {
            list = (List) listeHistorique;
        } else {
            list = new ArrayList(listeHistorique);
        }
        return list;
    }

}
