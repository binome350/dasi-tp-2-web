/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Actions;

import com.mycompany.gustatifweb.ActionServlet;
import static com.mycompany.gustatifweb.ActionServlet.connexionLivreurPage;
import static com.mycompany.gustatifweb.ActionServlet.mainPageLivreur;
import static com.mycompany.gustatifweb.ActionServlet.typeLivreur;
import static com.mycompany.gustatifweb.ActionServlet.typeUser;
import static com.mycompany.gustatifweb.ActionServlet.userIDAttribute;
import static com.mycompany.gustatifweb.ActionServlet.usernameAttribute;
import static com.mycompany.gustatifweb.ActionServlet.usertypeAttribute;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import metier.modele.Client;
import metier.modele.Commande;
import metier.modele.Livreur;
import metier.modele.Produit;
import metier.modele.Restaurant;
import metier.service.ServiceMetier;

/**
 *
 * @author Vincent Falconieri
 */
public class ActiviteLivreur {

    ServiceMetier service;

    public ActiviteLivreur(ServiceMetier serviceMetiers) {
        service = serviceMetiers;
    }

    /**
     * Permet de connecter un livreur sur l'application.
     *
     * @param session
     * @param request
     * @return true si s'est correctement déroulée, false sinon. Ajout d'un
     * message d'erreur dans la requête via un attribut "error"
     */
    public boolean connecterLivreur(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        //Récupère les ID/mdp
        String usernameFormulaire = request.getParameter("identifiant");
        String mdp = request.getParameter("motdepasse");

        //DEBUG ZONE //
        System.err.println("user :" + usernameFormulaire);
        System.err.println("mdp :" + mdp);
        //FIN DEBUG ZONE //

        //On check si le livreur existe
        Livreur livreurObject = service.trouverLivreur(usernameFormulaire);

        if (livreurObject != null) { //L'user existe
            //On ajoute des informations à sa session  = on le connecte
            session.setAttribute(usernameAttribute, usernameFormulaire); //On lui donne un nom
            session.setAttribute(usertypeAttribute, typeLivreur); //on lui donne un type
            session.setAttribute(userIDAttribute, livreurObject.getID()); //on lui attribut son objet de la BD

            //On transmet un message d'authentification
            ActionServlet.setMessage(request, "Le livreur a été connecté.", ActionServlet.mainPageLivreur);

        } else { //L'user n'existe pas.
            ActionServlet.setError(request, "Le livreur spécifié n'existe pas.", ActionServlet.connexionLivreurPage);
            enErreur = true;
        }

        return enErreur;
    }

    /* Permet de récupérer les détails de la commande courante d'un livreur pour la cloturer
     *
     * @param session
     * @param request
     * @return true si s'est correctement déroulée, false sinon. Ajout d'un
     * message d'erreur dans la requête via un attribut "error"
     */
    public boolean getCommandeALivrer(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        //Récupère les informations
        long idLivreurCommande = (long) session.getAttribute(userIDAttribute);
        Livreur livreurCommande = null;
        Commande commandeCourante = null;
        ActiviteCommande activiteCommandes = new ActiviteCommande(service);
        Collection<Commande> coll = null;
        List<Commande> list = null;

        //Récupération de l'utilisateur
        try {
            livreurCommande = service.trouverLivreurParId(idLivreurCommande);
            //DEBUG //
            System.err.println("Le livreur trouvé est : " + livreurCommande);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteLivreur : " + ex);
            ActionServlet.setError(request, "ERREUR : ID de livreur inconnu.", ActionServlet.indexPage);
            enErreur = true;
        }

        //Conversion Collection en Liste
        coll = livreurCommande.getCommandes();
        if (coll instanceof List) {
            list = (List) coll;
        } else {
            list = new ArrayList(coll);
        }

        //Si livreur disponible
        if (livreurCommande.isDisponible()) {
            ActionServlet.setError(request, "Vous n'avez aucune commande à livrer.", ActionServlet.mainPageLivreur);
            enErreur = true;
        }

        //Récupération de la commande
        try {
            commandeCourante = activiteCommandes.getLastCommandeEnCours(list);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteLivreur : " + ex);
            ActionServlet.setError(request, "ERREUR : impossible de récupérer la dernière commande en cours.", ActionServlet.indexPage);
            enErreur = true;
        }

        //Si livreur disponible
        if (commandeCourante != null && commandeCourante.getLivreur() != null && commandeCourante.getLivreur().isDisponible()) {
            ActionServlet.setError(request, "(Livreur de la commande disponible) Vous n'avez aucune commande à livrer.", ActionServlet.mainPageLivreur);
            enErreur = true;
        }

        //Envoi à la Servlet par request des attributs nécessaires.
        if (!enErreur && commandeCourante != null) {
            //TODO = A mettre dans une méthode à part ? 
            request.setAttribute(ActionServlet.attrCommande, commandeCourante);

            //Enpaquetage des données dans la requête
            ActionServlet.setMessage(request, "Les détails de votre commande à livrer ont été correctement récupérés.", ActionServlet.clientConfirmationCommande);
        } else if (commandeCourante == null) { //La commande n'existe pas.
            ActionServlet.setError(request, "Vous n'avez aucune commande à livrer.", ActionServlet.mainPageLivreur);
            enErreur = true;
        }

        return enErreur;
    }

    /**
     * Permet de valider une commande.
     *
     * @param session
     * @param request
     * @return true si s'est correctement déroulée, false sinon. Ajout d'un
     * message d'erreur dans la requête via un attribut "error"
     */
    public boolean cloturerLivraison(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        //Récupère les informations
        String idCommandeSTRING = request.getParameter("idCommandeACloturer");
        long idCommandeLONG = -1;
        Commande commandeCourante = null;

        //Parsing ID commande
        try {
            idCommandeLONG = Long.parseLong(idCommandeSTRING);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteLivreur : " + ex);
            ActionServlet.setError(request, "ERREUR : validation de commande impossible, ID invalide.", ActionServlet.clientListeHistorique);
            enErreur = true;
        }

        //Récupération de la commande
        try {
            commandeCourante = service.trouverCommandeParId(idCommandeLONG);
            //DEBUG //
            System.err.println("La commande trouvée est : " + commandeCourante);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteLivreur : " + ex);
            ActionServlet.setError(request, "ERREUR : ID de commande inconnu.", ActionServlet.indexPage);
            enErreur = true;
        }

        //Validation de la commande
        boolean estCloture = false;
        try {
            estCloture = service.conclureCommande(commandeCourante);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteLivreur : " + ex);
            ActionServlet.setError(request, "ERREUR : validation de la commande impossible.", ActionServlet.indexPage);
            enErreur = true;
        }

        if (!enErreur && estCloture) {
            //On transmet un message d'authentification
            ActionServlet.setMessage(request, "La commande a été marquée comme livrée et cloturée.", ActionServlet.mainPageLivreur);
        } else { //L'user n'existe pas.
            ActionServlet.setError(request, "La commande n'a pas été marquée comme livrée et cloturée.", ActionServlet.mainPageLivreur);
            enErreur = true;
        }

        return enErreur;
    }

    public boolean getDetailsLivreur(HttpSession session, HttpServletRequest request) {
        boolean enErreur = false;

        //Récupère les informations
        Long idLivreur = Long.parseLong(request.getParameter("id"));
        Livreur livreurDemande = null;

        //DEBUG //
        System.err.println("ActiviteLivreur - ID dans print Livreur : " + idLivreur);

        //Récupération du restaurant
        try {
            livreurDemande = service.trouverLivreurParId(idLivreur);
            //DEBUG //
            System.err.println("ActiviteLivreur - Le livreur trouvé est : " + livreurDemande);
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteLivreur : " + ex);
            ActionServlet.setError(request, "ERREUR : ID de livreur inconnu.", ActionServlet.mainPageAdmin);
            enErreur = true;
        }

        //Envoi à la Servlet par request des attributs nécessaires.
        if (!enErreur && livreurDemande != null) {
            //TODO = A mettre dans une méthode à part ? 
            request.setAttribute(ActionServlet.attrLivreur, livreurDemande);
            //Enpaquetage des données dans la requête
            ActionServlet.setMessage(request, "Les détails du livreur demandé ont été correctement récupérés.", ActionServlet.mainPageAdmin);
        } else if (livreurDemande == null) {
            ActionServlet.setError(request, "ERREUR : aucun livreur ne correspond à cet ID.", ActionServlet.mainPageAdmin);
            enErreur = true;
        }

        return enErreur;
    }

}
