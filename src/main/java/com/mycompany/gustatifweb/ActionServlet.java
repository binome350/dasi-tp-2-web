/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gustatifweb;

import Actions.ActiviteRestaurant;
import Actions.ActiviteClient;
import Actions.ActiviteCommande;
import Actions.ActiviteLivreur;
import Actions.ActiviteProduit;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.google.gson.JsonArray;
import dao.JpaUtil;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.ws.rs.client.ClientBuilder;
import metier.modele.Client;
import metier.modele.Commande;
import metier.modele.Livreur;
import metier.modele.Produit;
import metier.modele.Restaurant;
import metier.service.ServiceMetier;
import util.exception.LivreurUnavailableException;

/**
 *
 * @author Vincent Falconieri
 */
@WebServlet(name = "ActionServlet", urlPatterns = {"/ActionServlet"})
public class ActionServlet extends HttpServlet {

    //Paramètres de la classe
    ServiceMetier serviceMetiers;
    //ID utilisés pour les attributes de la session
    public static final String usernameAttribute = "username"; //Son nom
    public static final String usertypeAttribute = "usertype"; //Son type
    public static final String userIDAttribute = "userID"; //son objet
    public static final String userRestauCourantAttribute = "choixRestauActuel"; //le restaurant actuellement dans sa commande
    //Type qui aurait peuvent être remplacer par un typeDef, si nécessaire.
    public static final String typeUser = "user"; //Le nom d'un type utilisateur
    public static final String typeLivreur = "livreur"; //Le nom d'un type livreur
    public static final String typeAdmin = "admin"; //Le nom d'un type administrateur

//Les noms des attributs dans la requête pour notifier d'erreur venant des activités
    public static final String attrErreur = "error";
    public static final String attrMessage = "message";
    public static final String attrPageToRedirect = "pageToRedirect";
    public static final String attrRestaurant = "restaurant";
    public static final String attrListeRestaurant = "ListeRestaurants";
    public static final String attrProduit = "produit";
    public static final String attrListeProduit = "ListeProduits";
    public static final String attrCommande = "commande";
    public static final String attrListeCommande = "ListeCommandes";
    public static final String attrClient = "client";
    public static final String attrListeClient = "ListeClients";
    public static final String attrLivreur = "client";
    public static final String attrListeLivreur = "ListeClients";

    //Pages web de l'application
    public static final String indexPage = "./index.html";

    public static final String inscriptionPage = "./inscription.html";

    public static final String connexionClientPage = "./connexionClient.html";
    public static final String connexionLivreurPage = "./connexionLivreur.html";

    public static final String clientListeRestaurants = "./clientListeRestaurants.html";
    public static final String clientListeHistorique = "./clientListeHistorique.html";
    public static final String clientListeProduits = "./clientListeProduits.html";

    public static final String clientConfirmationCommande = "./clientConfirmationCommande.html";
    public static final String livreurClotureLivraison = "./livreurClotureLivraison.html";

    public static final String mainPageClient = "./mainPageClient.html";
    public static final String mainPageLivreur = "./livreurClotureLivraison.html";
    public static final String mainPageAdmin = "./mainPageAdmin.html";

    @Override
    public void init() throws ServletException {
        super.init();
        JpaUtil.init();
        serviceMetiers = new ServiceMetier();
    }

    @Override
    public void destroy() {
        JpaUtil.destroy();
        super.destroy();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //On active la session dès le départ.
        HttpSession session = request.getSession(true); //session

        //On gère les problèmes d'encodage
        request.setCharacterEncoding("UTF-8"); //encodage
        response.setContentType("text/html;charset=UTF-8");

        // === Récupère paramètres ===
        //On récupère l'action qui veut être effectuée
        String todoParameter = request.getParameter("todo");
        //On récupère l'user de la session (son nom et son type)
        String sessionUserNameAttribute = (String) session.getAttribute(usernameAttribute);
        String sessionUserTypeAttribute = (String) session.getAttribute(usertypeAttribute);

        // === Déclarations nécessaires des activités ===
        ActiviteRestaurant activiteRestau = new ActiviteRestaurant(serviceMetiers);
        ActiviteProduit activiteProduits = new ActiviteProduit(serviceMetiers);
        ActiviteCommande activiteCommandes = new ActiviteCommande(serviceMetiers);
        ActiviteClient activiteClients = new ActiviteClient(serviceMetiers);
        ActiviteLivreur activiteLivreur = new ActiviteLivreur(serviceMetiers);

        //DEBUG ZONE
        printParameter(request); //Afficher les paramètres de la requête
        System.err.println("sessionUserNameAttribute - parameter : " + sessionUserNameAttribute);
        System.err.println("sessionUserTypeAttribute - parameter : " + sessionUserTypeAttribute);
        System.err.println("todoParameter : " + todoParameter);
        //FIN DEBUG ZONE

        if (todoParameter != null) { // Il y a quelques chose à faire

            if (estLogged(sessionUserTypeAttribute)) {
                // ==== UTILISATEUR CONNECTED =====
                switch (todoParameter) {
                    case "inscription":
                        // ========================= PROCEDURE D'Inscription ============================
                        Formatage.Formatage.formatErreur("ERREUR : Vous êtes déjà connecté.", indexPage, response.getWriter());
                        // ========================= FIN DE PROCEDURE D'Inscription =====================

                        break;
                    case "connexionClient":
                        // ========================= PROCEDURE DE LOGIN USER ============================
                        redirectSuivantUser(sessionUserTypeAttribute, response);
                        // ========================= FIN PROCEDURE DE LOGIN USER ========================
                        break;
                    case "connexionLivreur":
                        // ========================= PROCEDURE DE LOGIN LIVREUR =========================
                        redirectSuivantUser(sessionUserTypeAttribute, response);
                        // ========================= FIN PROCEDURE DE LOGIN LIVREUR =====================
                        break;
                    case "deconnexion":
                        // ========================= PROCEDURE DE LOGOUT ================================
                        if (!activiteClients.deconnecterClient(session, request)) { //Le client est deconnecté
                            Formatage.Formatage.formatMessage((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // ========================= FIN PROCEDURE DE LOGOUT ============================
                        break;

                    //======================================== PRISE DE COMMANDE ET ASSIMILE //========================================
                    case "listeRestaurants":
                        // ========================= PROCEDURE DE RECUPERATION DE LA LISTE DES RESTAURANTS =================================
                        if (!activiteRestau.listerRestaurants(session, request)) { //le restaurant a bien été stocké
                            //SPECIAL = Envoi de données, la liste des produits, récupéré depuis la requête
                            Formatage.Formatage.formatListeRestaurants((List<Restaurant>) request.getAttribute(attrListeRestaurant), (String) request.getAttribute(attrMessage), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // ========================= FIN DE PROCEDURE DE RECUPERATION DE LA LISTE DES RESTAURANTS  =================================
                        break;
                    case "setRestaurantCourant":
                        // ========================= PROCEDURE D'AJOUT D'UN RESTAURANT ===================
                        if (!activiteRestau.setRestaurantCourant(session, request)) { //le restaurant a bien été stocké
                            Formatage.Formatage.formatMessage((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // ========================= FIN PROCEDURE D'AJOUT D'UN RESTAURANT ===============
                        break;
                    case "listeProduits":
                        // ========================= PROCEDURE D'ENVOIE DES PRODUITS D'UN RESTAURANT =====
                        if (!activiteProduits.listerProduits(session, request)) { //le restaurant a bien été stocké
                            //SPECIAL = Envoi de données, la liste des produits, récupéré depuis la requête
                            Formatage.Formatage.formatUnRestaurantEtProduits((Restaurant) request.getAttribute(attrRestaurant), (List<Produit>) request.getAttribute(attrListeProduit), (String) request.getAttribute(attrMessage), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // ========================= FIN PROCEDURE D'ENVOIE DES PRODUITS D'UN RESTAURANT =
                        break;
                    case "passageCommande":
                        // ========================= PROCEDURE DE TERMINAISON DE COMMANDE ================
                        if (!activiteCommandes.passerCommande(session, request)) { //la commande a bien été passée
                            Formatage.Formatage.formatMessage((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // =========================FIN DE PROCEDURE DE TERMINAISON DE COMMANDE ==========
                        break;
                    case "detailCommande":
                        // ========================= PROCEDURE D'ENVOIE DES DETAILS D'UNE COMMANDE =======
                        if (!activiteCommandes.getDetailsLastCommande(session, request)) { //la commande a bien été passée
                            Formatage.Formatage.formatUneCommande((Commande) request.getAttribute(attrCommande), (Restaurant) request.getAttribute(attrRestaurant), (String) request.getAttribute(attrMessage), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // ========================= PROCEDURE D'ENVOIE DES DETAILS D'UNE COMMANDE =======
                        break;

                    //======================================== HISTORIQUE ET ASSIMILE //========================================
                    case "listeHistorique":
                        // ========================= PROCEDURE D'ENVOIE DE L'HISTORIQUE DES COMMANDES =======
                        if (!activiteClients.getHistoriqueCommande(session, request)) { //On a bien récupéré les details du restaurant
                            Formatage.Formatage.formatListeHistorique((List<Commande>) request.getAttribute(attrListeCommande), (String) request.getAttribute(attrMessage), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // ========================= PROCEDURE D'ENVOIE DE L'HISTORIQUE DES COMMANDES =======
                        break;
                    case "detailCommandeSpecifique":
                        // ========================= PROCEDURE D'ENVOIE DES DETAILS D'UNE COMMANDE =======
                        if (!activiteCommandes.getDetailsUneCommande(session, request)) { //On a bien récupéré les details du restaurant
                            Formatage.Formatage.formatUneCommande((Commande) request.getAttribute(attrCommande), (Restaurant) request.getAttribute(attrRestaurant), (String) request.getAttribute(attrMessage), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // ========================= PROCEDURE D'ENVOIE DES DETAILS D'UNE COMMANDE =======
                        break;
                    case "getCommandeALivrer":
                        // ========================= PROCEDURE DE VALIDATION DE LIVRAISON =======
                        if (!activiteLivreur.getCommandeALivrer(session, request)) { //On a bien récupéré les details du restaurant
                            Formatage.Formatage.formatUneClotureLivraison((Commande) request.getAttribute(attrCommande), (String) request.getAttribute(attrMessage), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // ========================= FIN PROCEDURE DE VALIDATION DE LIVRAISON =======
                        break;
                    case "clotureLivraison":
                        // ========================= PROCEDURE DE VALIDATION DE LIVRAISON =======
                        if (!activiteLivreur.cloturerLivraison(session, request)) { //On a bien récupéré les details du restaurant
                            Formatage.Formatage.formatMessage((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // ========================= FIN PROCEDURE DE VALIDATION DE LIVRAISON =======
                        break;

                    //======================================== Demandes académiques //========================================
                    case "detaillerRestaurant": //OLD POUR LE TEST DEMANDé
                        // ========================= PROCEDURE D'ENVOIE DES DETAILS D'UN RESTAURANT =======
                        Formatage.Formatage.formatErreur("ERREUR : détails de restaurant impossible, vous êtes connecté.", indexPage, response.getWriter());
                        // ========================= FIN DE PROCEDURE D'ENVOIE DES DETAILS D'UN RESTAURANT =======
                        break;
                    case "gestionAdmin":
                        // ========================= PROCEDURE RECUPERATION DONNES ADMINISTRATION =======
                        Formatage.Formatage.formatErreur("ERREUR : accès aux détails d'administrateur non autorisé, vous êtes connecté en tant qu'utilisateur.", indexPage, response.getWriter());
                        // ========================= FIN DE PROCEDURE RECUPERATION DONNES ADMINISTRATION =======
                        break;
                }

            } else {
                // ==== UTILISATEUR NON CONNECTE ====
                switch (todoParameter) {
                    case "inscription":
                        // ========================= PROCEDURE D'Inscription ============================
                        if (!activiteClients.inscrireClient(session, request)) { //Le client est inscrit
                            Formatage.Formatage.formatMessage((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // ========================= FIN DE PROCEDURE D'Inscription =====================
                        break;
                    case "connexionClient":
                        // ========================= PROCEDURE DE LOGIN USER ============================
                        if (!activiteClients.connecterClient(session, request)) { //Le client est connecté
                            Formatage.Formatage.formatMessage((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // ========================= FIN PROCEDURE DE LOGIN USER =========================
                        break;
                    case "connexionLivreur":
                        // ========================= PROCEDURE DE LOGIN LIVREUR ==========================
                        if (!activiteLivreur.connecterLivreur(session, request)) { //Le client est connecté
                            Formatage.Formatage.formatMessage((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // ========================= FIN PROCEDURE DE LOGIN LIVREUR ======================
                        break;
                    case "deconnexion":
                        // ========================= PROCEDURE DE LOGOUT =================================
                        Formatage.Formatage.formatErreur("ERREUR : déconnexion impossible, vous n'êtes pas connecté.", indexPage, response.getWriter());
                        // ========================= FIN PROCEDURE DE LOGOUT =============================
                        break;
                    //======================================== PRISE DE COMMANDE ET ASSIMILE //========================================
                    case "listeRestaurants":
                        // ========================= PROCEDURE DE RECUPERATION DE LA LISTE DES RESTAURANTS 
                        Formatage.Formatage.formatErreur("ERREUR : récupération de la liste des restaurants impossible, vous n'êtes pas connecté.", indexPage, response.getWriter());
                        // ========================= FIN DE PROCEDURE DE RECUPERATION DE LA LISTE DES RESTAURANTS 
                        break;
                    case "setRestaurantCourant":
                        // ========================= PROCEDURE D'AJOUT D'UN RESTAURANT ===================
                        Formatage.Formatage.formatErreur("ERREUR : choix de restaurant impossible, vous n'êtes pas connecté.", indexPage, response.getWriter());
                        // ========================= FIN PROCEDURE D'AJOUT D'UN RESTAURANT ===============
                        break;
                    case "listeProduits":
                        // ========================= PROCEDURE D'ENVOIE DES PRODUITS D'UN RESTAURANT =====
                        Formatage.Formatage.formatErreur("ERREUR : choix de produits impossible, vous n'êtes pas connecté.", indexPage, response.getWriter());
                        // ========================= FIN PROCEDURE D'ENVOIE DES PRODUITS D'UN RESTAURANT =
                        break;
                    case "passageCommande":
                        // ========================= PROCEDURE DE TERMINAISON DE COMMANDE ================
                        Formatage.Formatage.formatErreur("ERREUR : Impossible de passer la commande, vous n'êtes pas connecté.", indexPage, response.getWriter());
                        // =========================FIN DE PROCEDURE DE TERMINAISON DE COMMANDE ==========
                        break;
                    case "detailCommande":
                        // ========================= PROCEDURE D'ENVOIE DES DETAILS D'UNE COMMANDE =======
                        Formatage.Formatage.formatErreur("ERREUR : Impossible d'afficher les détails de votre dernière commande, vous n'êtes pas connecté.", indexPage, response.getWriter());
                        // ========================= PROCEDURE D'ENVOIE DES DETAILS D'UNE COMMANDE =======
                        break;
                    case "getCommandeALivrer":
                        // ========================= PROCEDURE DE VALIDATION DE LIVRAISON =======
                        Formatage.Formatage.formatErreur("ERREUR : Impossible d'accéder à votre espace personnel de livreur, vous n'êtes pas connecté.", indexPage, response.getWriter());
                        // ========================= FIN PROCEDURE DE VALIDATION DE LIVRAISON =======
                        break;
                    case "clotureLivraison":
                        // ========================= PROCEDURE DE VALIDATION DE LIVRAISON =======
                        Formatage.Formatage.formatErreur("ERREUR : Impossible de valider une livraison, vous n'êtes pas connecté.", indexPage, response.getWriter());
                        // ========================= FIN PROCEDURE DE VALIDATION DE LIVRAISON =======
                        break;
                    //======================================== HISTORIQUE ET ASSIMILE //========================================
                    case "listeHistorique":
                        // ========================= PROCEDURE D'ENVOIE DE L'HISTORIQUE DES COMMANDES =======
                        Formatage.Formatage.formatErreur("ERREUR : Impossible d'afficher votre historique de commande, vous n'êtes pas connecté.", indexPage, response.getWriter());
                        // ========================= PROCEDURE D'ENVOIE DE L'HISTORIQUE DES COMMANDES =======
                        break;
                    case "detailCommandeSpecifique":
                        // ========================= PROCEDURE D'ENVOIE DES DETAILS D'UNE COMMANDE =======
                        Formatage.Formatage.formatErreur("ERREUR : Impossible d'afficher les détails d'une commande, vous n'êtes pas connecté.", indexPage, response.getWriter());
                        // ========================= PROCEDURE D'ENVOIE DES DETAILS D'UNE COMMANDE =======
                        break;
                    //======================================== ADMINISTRATIF //========================================
                    case "gestionAdmin":
                        // ========================= PROCEDURE RECUPERATION DONNES ADMINISTRATION =======
                        if (!activiteCommandes.getDetailsAdministration(session, request)) { //On a bien récupéré les details du restaurant
                            //Un peu spécial, car pas de cast sur les listes avec héritages implémentée facilement. Donc la conditon dans le if ne vaut "rien".
                            Formatage.Formatage.formatDetailsAdministration(serviceMetiers, (String) request.getAttribute(attrMessage), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // ========================= FIN DE PROCEDURE RECUPERATION DONNES ADMINISTRATION =======
                        break;
                    case "detaillerRestaurant":
                        // ========================= PROCEDURE D'ENVOIE DES DETAILS D'UN RESTAURANT =======
                        if (!activiteRestau.getDetailsRestaurant(session, request)) { //On a bien récupéré les details du restaurant
                            Formatage.Formatage.formatUnRestaurant((Restaurant) request.getAttribute(attrRestaurant), (String) request.getAttribute(attrMessage), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // ========================= FIN DE PROCEDURE D'ENVOIE DES DETAILS D'UN RESTAURANT =======
                        break;
                    case "detaillerLivreur":
                        // ========================= PROCEDURE D'ENVOIE DES DETAILS D'UN CLIENT =======
                        if (!activiteLivreur.getDetailsLivreur(session, request)) { //On a bien récupéré les details du restaurant
                            Formatage.Formatage.formatUnLivreur((Livreur) request.getAttribute(attrLivreur), (String) request.getAttribute(attrMessage), response.getWriter());
                        } else { //Il y a eu une erreur
                            Formatage.Formatage.formatErreur((String) request.getAttribute(attrMessage), (String) request.getAttribute(attrPageToRedirect), response.getWriter());
                        }
                        // ========================= FIN DE PROCEDURE D'ENVOIE DES DETAILS D'UN CLIENT =======
                        break;
                }
            }

            // TO MODIFY AND TO ADAPT TO MAKE IT BETTER LIKE JUST ABOVE =) 
            switch (todoParameter) {

                case "clotureLivraison":

                    String typeDuLivreur = (String) session.getAttribute(usertypeAttribute);
                    long idDuLiveur = (long) session.getAttribute(userIDAttribute);

                    if (typeDuLivreur != "" | idDuLiveur < 0) {
                        if (typeLivreur == typeDuLivreur) {
                            Livreur livreurCourant = serviceMetiers.trouverLivreurParId(idDuLiveur);
                            if (livreurCourant.isDisponible()) { // Si il est disponible, il n'a pas de de commande en cours
                                //TODO Formatage.Formatage.formatAucuneLivraison(response.getWriter());
                            } else { // Il y a une commande en cours
                                //TODO Commande lastCommande = getLastCommande(livreurCourant.getCommandes());
                                //TODO Formatage.Formatage.formatUneLivraison(livreurCourant, lastCommande, response.getWriter());
                            }
                        } else {
                            Formatage.Formatage.formatErreur("Erreur : vous n'êtes pas connecté en tant que livreur.", indexPage, response.getWriter());
                        }
                    } else {
                        Formatage.Formatage.formatErreur("Erreur : vous n'êtes pas connecté.", connexionLivreurPage, response.getWriter());
                    }
                    break;

            }
        }
    }

    protected void redirectSuivantUser(String typeUser, HttpServletResponse response) throws IOException {
        if (typeUser.equals(typeUser)) {
            //C'est un user, on le renvoit vers sa page de gestion personnelle
            Formatage.Formatage.formatErreur("Vous êtes déjà connecté en tant qu'utilisateur.", mainPageClient, response.getWriter());
        } else if (typeUser.equals(typeLivreur)) {
            //C'est un admin, on l'envoit faire la page de gestion
            Formatage.Formatage.formatErreur("Vous êtes déjà connecté en tant que livreur.", mainPageLivreur, response.getWriter());
        } else if (typeUser.equals(typeAdmin)) {
            //C'est un admin, on l'envoit faire la page de gestion
            Formatage.Formatage.formatErreur("Vous êtes déjà connecté en tant qu'admin.", mainPageAdmin, response.getWriter());
        } else {
            //Nom inconnu, on le renvoit vers la page de connexion
            Formatage.Formatage.formatErreur("Vous êtes déjà connecté en tant qu'inconnu.", indexPage, response.getWriter());
        }
    }

    /**
     * Fonction qui vérifie si le typeUser passé en paramètre rend la session
     * "logguée"
     *
     * @param typeUser
     * @return
     */
    protected boolean estLogged(String typeUser) {
        if (typeUser != null) {
            if (typeUser.equals(this.typeUser) | typeUser.equals(typeLivreur) | typeUser.equals(typeAdmin)) {
                //Le typeUser est connu
                return true;
            } else {
                System.err.println("WARNING - ERREUR : L'utilisateur est logguée avec un type inconnu : " + typeUser);
            }
        }
        return false;
    }

    protected void sendError(HttpServletResponse response, String error) {
        String jsonObject = "{ error: 'yes', errorMessage: '" + error + "' }";
        response.setContentType("application/json"); // Get the printwriter object from response to write the required json object to the output stream
        PrintWriter out = null;
        try {
            out = response.getWriter(); // Assuming your json object is **jsonObject**, perform the following, it will return your json object
            out.print(jsonObject);
        } catch (Exception ex) {
            System.err.println("Servlet : Error = " + ex);
        }
    }

    protected void sendAnswer(HttpServletResponse response, String message) {
        String jsonObject = "{ error: 'no', Message: '" + message + "' }";
        response.setContentType("application/json"); // Get the printwriter object from response to write the required json object to the output stream
        PrintWriter out = null;
        try {
            out = response.getWriter(); // Assuming your json object is **jsonObject**, perform the following, it will return your json object
            out.print(jsonObject);
        } catch (Exception ex) {
            System.err.println("Servlet : Error = " + ex);
        }
    }

    /**
     * Fonction de DEBUG permettant de print tous les paramètres de la requête.
     *
     * @param request
     */
    protected void printParameter(HttpServletRequest request) {
        //récupérer l'énumération des paramètres de la requête
        Enumeration params = request.getParameterNames();
        //Print chacun des paramètres dans la console. (flux standard)
        while (params.hasMoreElements()) {
            String paramName = (String) params.nextElement();
            System.out.println("Parameter Name - " + paramName + ", Value - " + request.getParameter(paramName));
        }
    }

    /**
     * Permet de rajouter une erreur à la requête courante, pour notifier d'un
     * mauvais traitement. (Erreur interne)
     *
     * @param request
     * @param message : le message d'erreur renvoyé
     * @param pageToRedirect : la page de redirection attendue suite à l'erreur.
     */
    static public void setError(HttpServletRequest request, String message, String pageToRedirect) {
        request.setAttribute(attrErreur, true);
        request.setAttribute(attrMessage, message);
        request.setAttribute(attrPageToRedirect, pageToRedirect);
    }

    /**
     * Permet de rajouter un message à la requête courante, suite à un
     * traitement, une fois terminé.
     *
     * @param request
     * @param message : le message renvoyé
     * @param pageToRedirect : la page de redirection attendue suite au
     * traitement.
     */
    static public void setMessage(HttpServletRequest request, String message, String pageToRedirect) {
        request.setAttribute(attrErreur, false);
        request.setAttribute(attrMessage, message);
        request.setAttribute(attrPageToRedirect, pageToRedirect);
    }

    /**
     * Permet de remettre à zéro cles champs erreur ou message.
     *
     * @param request
     */
    static public void resetErrorOuMessage(HttpServletRequest request) {
        request.setAttribute(attrErreur, null);
        request.setAttribute(attrMessage, null);
        request.setAttribute(attrPageToRedirect, null);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
