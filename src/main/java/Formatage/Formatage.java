package Formatage;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mycompany.gustatifweb.ActionServlet;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import metier.modele.Client;
import metier.modele.Commande;
import metier.modele.CommandeProduit;
import metier.modele.Livreur;
import metier.modele.Produit;
import metier.modele.Restaurant;
import metier.service.ServiceMetier;

/**
 *
 * @author Vincent Falconieri
 */
public class Formatage {

    public static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    /**
     * Récupérer un objet json parsant le message passé en paramètre
     *
     * @param message
     * @return
     */
    protected static JsonObject getJsonMessageNormal(String message) {
        JsonObject jsonMessage = new JsonObject();
        jsonMessage.addProperty("error", false);
        jsonMessage.addProperty("message", message);

        //Renvoi de l'objet json
        return jsonMessage;
    }

    /**
     * Récupérer un objet json parsant le restaurant passé en paramètre
     *
     * @param restaurantToParse
     * @return
     */
    protected static JsonObject getJsonRestaurant(Restaurant restaurantToParse) {
        JsonObject jsonRestaurant = new JsonObject();

        if (restaurantToParse != null) { //Si on a bien un restaurant
            jsonRestaurant.addProperty("id", restaurantToParse.getId());
            jsonRestaurant.addProperty("denomination", restaurantToParse.getDenomination());
            jsonRestaurant.addProperty("description", restaurantToParse.getDescription());
            jsonRestaurant.addProperty("adresse", restaurantToParse.getAdresse());
            jsonRestaurant.addProperty("lat", restaurantToParse.getLatitude());
            jsonRestaurant.addProperty("long", restaurantToParse.getLongitude());
        }
        //Renvoi de l'objet json
        return jsonRestaurant;
    }

    /**
     * Récupérer un objet json parsant le produit passé en paramètre
     *
     * @param produitToParse
     * @param restaurantToParse
     * @return
     */
    protected static JsonObject getJsonProduit(Produit produitToParse) {
        JsonObject jsonProduits = new JsonObject();

        if (produitToParse != null) { //Si on a bien un restaurant
            jsonProduits.addProperty("id", produitToParse.getId());
            jsonProduits.addProperty("denomination", produitToParse.getDenomination());
            jsonProduits.addProperty("description", produitToParse.getDescription());
            jsonProduits.addProperty("poids", produitToParse.getPoids());
            jsonProduits.addProperty("prix", produitToParse.getPrix());
        }
        //Renvoi de l'objet json
        return jsonProduits;
    }

    /**
     * Récupérer un objet json parsant le CommandeProduit passé en paramètre
     *
     * @param restaurantToParse
     * @return
     */
    protected static JsonObject getJsonCommandeProduit(CommandeProduit commandeProduitToParse) {
        JsonObject jsonCommandeProduits = null;

        if (commandeProduitToParse != null) { //Si on a bien un restaurant
            Produit produitToParse = commandeProduitToParse.getProduit();
            jsonCommandeProduits = getJsonProduit(produitToParse);
            jsonCommandeProduits.addProperty("quantite", commandeProduitToParse.getQuantite());
        }
        //Renvoi de l'objet json
        return jsonCommandeProduits;
    }

    /**
     * Récupérer un objet json parsant le commande passé en paramètre
     *
     * @param restaurantToParse
     * @return
     */
    protected static JsonObject getJsonCommande(Commande commandeToParse) {
        JsonObject jsonCommande = new JsonObject();

        if (commandeToParse != null) { //Si on a bien un restaurant
            Date dateEstimeeDATE = commandeToParse.getDate_estimee();
            Date dateCommandeDATE = commandeToParse.getDate_initiale();
            Date dateFinDATE = commandeToParse.getDate_fin();

            String dateEstimee = null;
            String dateCommande = null;
            String dateFin = null;

            if (dateEstimeeDATE != null) {
                dateEstimee = simpleDateFormat.format(dateEstimeeDATE);
            } else {
                dateEstimee = "Date Inconnue";
            }
            if (dateCommandeDATE != null) {
                dateCommande = simpleDateFormat.format(dateCommandeDATE);
            } else {
                dateCommande = "Date Inconnue";
            }
            if (dateFinDATE != null) {
                dateFin = simpleDateFormat.format(dateFinDATE);
            } else {
                dateFin = "Date Inconnue";
            }

            jsonCommande.addProperty("id", commandeToParse.getId());
            String etat = commandeToParse.getEtat();
            if (etat.equals("pending")) {
                etat = "En cours";
            } else {
                etat = "Reçue";
            }
            jsonCommande.addProperty("etat", etat);
            jsonCommande.addProperty("total", commandeToParse.getPrix());
            jsonCommande.addProperty("dateEstimee", dateEstimee);
            jsonCommande.addProperty("dateCommande", dateCommande);
            jsonCommande.addProperty("dateFin", dateFin);
            jsonCommande.addProperty("adresse", commandeToParse.getClient().getAdresse());
        }

        //Renvoi de l'objet json
        return jsonCommande;
    }

    protected static JsonObject getJsonClient(Client clientToParse) {
        JsonObject jsonClient = new JsonObject();

        if (clientToParse != null) { //Si on a bien un restaurant
            jsonClient.addProperty("nom", clientToParse.getNom());
            jsonClient.addProperty("prenom", clientToParse.getPrenom());
            jsonClient.addProperty("adresse", clientToParse.getAdresse());
        }

        //Renvoi de l'objet json
        return jsonClient;
    }

    protected static JsonObject getJsonLivreur(Livreur livreurToParse) {
        JsonObject jsonLivreur = new JsonObject();

        if (livreurToParse != null) { //Si on a bien un restaurant
            jsonLivreur.addProperty("id", livreurToParse.getID());
            jsonLivreur.addProperty("denomination", livreurToParse.getDenomination());
            jsonLivreur.addProperty("email", livreurToParse.getEmail());
            jsonLivreur.addProperty("lat", livreurToParse.getLatitude());
            jsonLivreur.addProperty("long", livreurToParse.getLongitude());
            jsonLivreur.addProperty("poidsMax", livreurToParse.getPoids_max());
            boolean etat = livreurToParse.isDisponible();
            String etatSortie = "inconnu";
            if (etat == true) {
                etatSortie = "disponible";
            } else {
                etatSortie = "indisponible";
            }
            jsonLivreur.addProperty("disponible", etatSortie);
        }
        //Renvoi de l'objet json
        return jsonLivreur;
    }

    /**
     * Envoi une liste de restaurant formaté en json sur la sortie fournie
     *
     * @param listeRestaurants
     * @param message
     * @param out
     * @return
     */
    public static boolean formatListeRestaurants(List<Restaurant> listeRestaurants, String message, PrintWriter out) {
        //==== FORMAT DU MESSAGE ==== 
        JsonArray jsonListeAnswer = new JsonArray();
        JsonObject jsonMessage = Formatage.getJsonMessageNormal(message);
        jsonListeAnswer.add(jsonMessage);

        //==== FORMAT DES RESTAURANTS ==== 
        JsonArray jsonListeRestaurants = new JsonArray();

        for (Restaurant r : listeRestaurants) {
            JsonObject jsonRestaurant = getJsonRestaurant(r);
            jsonListeRestaurants.add(jsonRestaurant);
        }

        //JSON Conteneur
        JsonObject contenair = new JsonObject();
        contenair.add("answer", jsonListeAnswer);
        contenair.add("restaurants", jsonListeRestaurants);

        //Serialisation et écriture sur le flux de sortie
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(contenair);

        //DEBUG //
        System.err.println("ENVOYE EN JSON (ListeRestaurants) : " + json);

        out.println(json);
        return true;
    }

    /**
     * Envoi un restaurant formaté en json sur la sortie fournie
     *
     * @param r
     * @param message
     * @param out
     * @return
     */
    public static boolean formatUnRestaurant(Restaurant r, String message, PrintWriter out) {
        //==== FORMAT DU MESSAGE ==== 
        JsonArray jsonListeAnswer = new JsonArray();
        JsonObject jsonMessage = Formatage.getJsonMessageNormal(message);
        jsonListeAnswer.add(jsonMessage);

        //==== FORMAT DU RESTAURANT ==== 
        JsonArray jsonListeRestaurants = new JsonArray();
        JsonObject jsonRestaurant = getJsonRestaurant(r);
        jsonListeRestaurants.add(jsonRestaurant);

        //JSON Conteneur
        JsonObject contenair = new JsonObject();
        contenair.add("answer", jsonListeAnswer);
        contenair.add("restaurant", jsonListeRestaurants);

        //Serialisation et écriture sur le flux de sortie
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(contenair);

        out.println(json);
        return true;
    }

    /**
     * Envoi une erreur formatée en json sur la sortie fournie
     *
     * @param message
     * @param pageToRedirect : Normalement en passant null comme argument,
     * aucune redirection n'est faite.
     * @param out
     * @return
     */
    public static boolean formatErreur(String message, String pageToRedirect, PrintWriter out) {
        JsonArray jsonListe = new JsonArray();
        JsonObject jsonMessage = new JsonObject();

        jsonMessage.addProperty("error", true);
        jsonMessage.addProperty("message", message);
        jsonMessage.addProperty("redirect", pageToRedirect);
        jsonListe.add(jsonMessage);

        //JSON Conteneur
        JsonObject contenair = new JsonObject();
        contenair.add("answer", jsonListe);

        //Serialisation et écriture sur le flux de sortie
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(contenair);

        //DEBUG //
        System.err.println("ENVOYE EN JSON (ERROR) : " + json);

        out.println(json);
        return true;
    }

    /**
     * Envoi un message formaté en json sur la sortie fournie
     *
     * @param message
     * @param pageToRedirect : Normalement en passant null comme argument,
     * aucune redirection n'est faite.
     * @param out
     * @return
     */
    public static boolean formatMessage(String message, String pageToRedirect, PrintWriter out) {
        JsonArray jsonListe = new JsonArray();
        JsonObject jsonMessage = new JsonObject();

        jsonMessage.addProperty("error", false);
        jsonMessage.addProperty("message", message);
        jsonMessage.addProperty("redirect", pageToRedirect);
        jsonListe.add(jsonMessage);

        //JSON Conteneur
        JsonObject contenair = new JsonObject();
        contenair.add("answer", jsonListe);

        //Serialisation et écriture sur le flux de sortie
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(contenair);

        //DEBUG //
        System.err.println("ENVOYE EN JSON (NORMAL) : " + json);

        out.println(json);
        return true;
    }

    /**
     * Envoi un restaurant et ses produits formatés en json sur la sortie
     * fournie
     *
     * @param r
     * @param listeProduits
     * @param message
     * @param out
     * @return
     */
    public static boolean formatUnRestaurantEtProduits(Restaurant r, List<Produit> listeProduits, String message, PrintWriter out) {
        //==== FORMAT DU MESSAGE ==== 
        JsonArray jsonListeAnswer = new JsonArray();
        JsonObject jsonMessage = Formatage.getJsonMessageNormal(message);
        jsonListeAnswer.add(jsonMessage);

        //==== FORMAT DU RESTAURANT ==== 
        JsonArray jsonListeRestaurants = new JsonArray();
        JsonObject jsonRestaurant = getJsonRestaurant(r);
        jsonListeRestaurants.add(jsonRestaurant);

        //==== FORMAT DES PRODUITS ==== 
        JsonArray jsonListeProduits = new JsonArray();

        for (Produit p : listeProduits) {
            JsonObject jsonProduits = getJsonProduit(p);
            jsonListeProduits.add(jsonProduits);
        }

        //JSON Conteneur
        JsonObject contenair = new JsonObject();
        contenair.add("answer", jsonListeAnswer);
        contenair.add("restaurants", jsonListeRestaurants);
        contenair.add("produits", jsonListeProduits);

        //Serialisation et écriture sur le flux de sortie
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(contenair);

        out.println(json);
        return true;
    }

    /**
     * Envoi une commande complète formatée en json sur la sortie fournie
     *
     * @param commandeCourante
     * @param r
     * @param message
     * @param out
     * @return
     */
    public static boolean formatUneCommande(Commande commandeCourante, Restaurant r, String message, PrintWriter out) {
        //==== FORMAT DU MESSAGE ==== 
        JsonArray jsonListeAnswer = new JsonArray();
        JsonObject jsonMessage = Formatage.getJsonMessageNormal(message);
        jsonListeAnswer.add(jsonMessage);

        // ==== FORMAT DU RESTAURANT ====
        JsonArray jsonListeRestaurants = new JsonArray();
        JsonObject jsonRestaurant = getJsonRestaurant(r);
        jsonListeRestaurants.add(jsonRestaurant);

        // ==== FORMAT DES PRODUITS =====
        JsonArray jsonListeProduits = new JsonArray();
        Collection<CommandeProduit> listeDesProduitsCommandes = commandeCourante.getProduits();

        for (CommandeProduit cp : listeDesProduitsCommandes) {
            JsonObject jsonProduits = getJsonCommandeProduit(cp);
            jsonListeProduits.add(jsonProduits);
        }

        // ==== FORMAT DE LA COMMANDE ==== (voir si après directement dans contenaire)
        JsonArray jsonInformationCommande = new JsonArray();
        JsonObject jsonCommande = getJsonCommande(commandeCourante);
        jsonInformationCommande.add(jsonCommande);

        // ==== AJOUT AU CONTENAIRE ====
        JsonObject contenair = new JsonObject();
        contenair.add("answer", jsonListeAnswer);
        contenair.add("restaurants", jsonListeRestaurants);
        contenair.add("produits", jsonListeProduits);
        contenair.add("informations", jsonInformationCommande);

        //Serialisation et écriture sur le flux de sortie
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(contenair);

        out.println(json);
        return true;
    }

    /**
     * Envoi un historique formaté en json sur la sortie fournie
     *
     * @param listeCommandes
     * @param message
     * @param out
     * @return
     */
    public static boolean formatListeHistorique(List<Commande> listeCommandes, String message, PrintWriter out) {
        //==== FORMAT DU MESSAGE ==== 
        JsonArray jsonListeAnswer = new JsonArray();
        JsonObject jsonMessage = Formatage.getJsonMessageNormal(message);
        jsonListeAnswer.add(jsonMessage);

        // ==== FORMAT DES COMMANDES =====
        JsonArray jsonListeCommandes = new JsonArray();

        for (Commande commande : listeCommandes) {
            JsonObject jsonCommande = getJsonCommande(commande);
            jsonListeCommandes.add(jsonCommande);
        }

        // ==== AJOUT AU CONTENAIRE ====
        JsonObject contenair = new JsonObject();
        contenair.add("answer", jsonListeAnswer);
        contenair.add("historique", jsonListeCommandes);

        //Serialisation et écriture sur le flux de sortie
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(contenair);

        out.println(json);
        return true;
    }

    /**
     * Envoi une cloture de livraison formatée en json sur la sortie fournie
     *
     * @param listeCommandes
     * @param message
     * @param out
     * @return
     */
    public static boolean formatUneClotureLivraison(Commande commandeCourante, String message, PrintWriter out) {
        Restaurant r = commandeCourante.getRestaurant();
        Livreur livreurCommande = commandeCourante.getLivreur();
        Client client = commandeCourante.getClient();

        //==== FORMAT DU MESSAGE ==== 
        JsonArray jsonListeAnswer = new JsonArray();
        JsonObject jsonMessage = Formatage.getJsonMessageNormal(message);
        jsonListeAnswer.add(jsonMessage);

        // ==== FORMAT DU RESTAURANT ====
        JsonArray jsonListeRestaurants = new JsonArray();
        JsonObject jsonRestaurant = getJsonRestaurant(r);
        jsonListeRestaurants.add(jsonRestaurant);

        // ==== FORMAT DES COMMANDES-PRODUITS =====
        JsonArray jsonListeProduits = new JsonArray();
        Collection<CommandeProduit> listeDesProduitsCommandes = commandeCourante.getProduits();

        for (CommandeProduit cp : listeDesProduitsCommandes) {
            JsonObject jsonProduits = getJsonCommandeProduit(cp);
            jsonListeProduits.add(jsonProduits);
        }

        // ==== FORMAT DE LA COMMANDE ==== (voir si après directement dans contenaire)
        JsonArray jsonInformationCommande = new JsonArray();
        JsonObject jsonCommande = getJsonCommande(commandeCourante);
        jsonInformationCommande.add(jsonCommande);

        // ==== FORMAT DU CLIENT ====
        JsonArray jsonInformationClient = new JsonArray();
        JsonObject jsonClient = getJsonClient(client);
        jsonInformationClient.add(jsonClient);

        // ==== FORMAT DU LIVREUR ====
        JsonArray jsonInformationLivreur = new JsonArray();
        JsonObject jsonLivreur = getJsonLivreur(livreurCommande);
        jsonInformationLivreur.add(jsonLivreur);

        // ==== AJOUT AU CONTENAIRE ====
        JsonObject contenair = new JsonObject();
        contenair.add("answer", jsonListeAnswer);
        contenair.add("restaurants", jsonListeRestaurants);
        contenair.add("produits", jsonListeProduits);
        contenair.add("livreur", jsonInformationLivreur);
        contenair.add("client", jsonInformationClient);
        contenair.add("informations", jsonInformationCommande);

        //Serialisation et écriture sur le flux de sortie
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(contenair);

        out.println(json);
        return true;
    }

    /**
     * Envoi toutes les listes disponibles pour l'administration formaté en json
     * sur la sortie fournie
     *
     * @param listeClients
     * @param listeCommandes
     * @param listeLivreurs
     * @param out
     * @param listeRestaurants
     * @param message
     */
    public static boolean formatDetailsAdministration(ServiceMetier service, String message, PrintWriter out) {
        List<Client> listeClients = null;
        List<Commande> listeCommandes = null;
        List<Livreur> listeLivreurs = null;
        List<Restaurant> listeRestaurants = null;

        //Récupère les informations
        try {
            listeClients = service.listeClients();
            listeCommandes = service.listeCommandesEnCours();
            listeLivreurs = service.listeLivreurs();
            listeRestaurants = service.listeRestaurants();
        } catch (Exception ex) {
            System.err.println("Erreur - ActiviteCommande : " + ex);
        }

        //==== FORMAT DU MESSAGE ==== 
        JsonArray jsonListeAnswer = new JsonArray();
        JsonObject jsonMessage = Formatage.getJsonMessageNormal(message);
        jsonListeAnswer.add(jsonMessage);

        // ==== FORMAT DES COMMANDES =====
        JsonArray jsonListeCommandes = new JsonArray();
        for (Commande commande : listeCommandes) {
            JsonObject jsonCommande = getJsonCommande(commande);
            jsonListeCommandes.add(jsonCommande);
        }

        //==== FORMAT DES RESTAURANTS ==== 
        JsonArray jsonListeRestaurants = new JsonArray();
        for (Restaurant r : listeRestaurants) {
            JsonObject jsonRestaurant = getJsonRestaurant(r);
            jsonListeRestaurants.add(jsonRestaurant);
        }

        // ==== FORMAT DU CLIENT ====
        JsonArray jsonListeClient = new JsonArray();
        for (Commande commande : listeCommandes) {
            JsonObject jsonClient = getJsonClient(commande.getClient());
            jsonListeClient.add(jsonClient);
        }
        // ==== FORMAT DU LIVREUR ====
        JsonArray jsonListeLivreur = new JsonArray();
        for (Livreur livreurCommande : listeLivreurs) {
            JsonObject jsonLivreur = getJsonLivreur(livreurCommande);
            jsonListeLivreur.add(jsonLivreur);
        }

        // ==== AJOUT AU CONTENAIRE ====
        JsonObject contenair = new JsonObject();
        contenair.add("answer", jsonListeAnswer);
        contenair.add("commandes", jsonListeCommandes);
        contenair.add("restaurants", jsonListeRestaurants);
        contenair.add("livreurs", jsonListeLivreur);
        contenair.add("clients", jsonListeClient);

        //Serialisation et écriture sur le flux de sortie
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(contenair);

        out.println(json);
        return true;
    }

    public static boolean formatUnLivreur(Livreur livreur, String message, PrintWriter out) {
        //==== FORMAT DU MESSAGE ==== 
        JsonArray jsonListeAnswer = new JsonArray();
        JsonObject jsonMessage = Formatage.getJsonMessageNormal(message);
        jsonListeAnswer.add(jsonMessage);

        //==== FORMAT DU RESTAURANT ==== 
        JsonArray jsonListeLivreur = new JsonArray();
        JsonObject jsonLivreur = getJsonLivreur(livreur);
        jsonListeLivreur.add(jsonLivreur);

        //JSON Conteneur
        JsonObject contenair = new JsonObject();
        contenair.add("answer", jsonListeAnswer);
        contenair.add("livreur", jsonListeLivreur);

        //Serialisation et écriture sur le flux de sortie
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(contenair);

        out.println(json);
        return true;
    }

}
